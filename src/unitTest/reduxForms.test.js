const InitialFeedback = require("../redux/forms");
test('should return the initial state', () => {
    expect(InitialFeedback).toMatchObject({
        firstname: '',
        lastname: '',
        telnum: '',
        email: '',
        agree: false,
        contactType: 'Tel.',
        message: ''
    })
  })
  