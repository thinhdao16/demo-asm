// ModalContainer.test.js
import { shallow } from 'enzyme';
import {Modal} from 'reactstrap';
import {DishDetail} from '../components/DishdetailComponent'
import React from 'react';

it('renders react-modal', () => {
  const wrapper = shallow(<DishDetail />);
  expect(wrapper.find(Modal)).toHaveLength(1);
});
it('opens modal when button is clicked', () => {
  const wrapper = shallow(<DishDetail />);
  expect(wrapper.find(Modal).prop('isOpen')).toBe(false);

  wrapper.find('button').simulate('click');
  expect(wrapper.find(Modal).prop('isOpen')).toBe(true);
});
it('renders childen when modal is open', () => {
  const wrapper = shallow(<Modal>modal content</Modal>);
  expect(wrapper.find(Modal).prop('children')).toBe('ModalBody');
});