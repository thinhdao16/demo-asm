import React, { lazy, Suspense } from "react";

const Component2 = lazy(() => import("./Component2"));
class Component1 extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isClicked: false
    };
  }
  click () {
    this.setState({ isClicked: true });
  };
  render() {
    return (
      <div>
        <div>component1 </div>
        <button onClick={this.click.bind(this)}>get component2</button>
        {this.state.isClicked && (
          <Suspense fallback={<div>loading...</div>}>
            <Component2 />
          </Suspense>
        )}
      </div>
    );
  }
}

export default Component1;
