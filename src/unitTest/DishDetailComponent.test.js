import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Component1 from './Component1';
Enzyme.configure({ adapter: new Adapter() })
describe('MyComponent', () => {
  it('should render correctly i mode', () => {
    const component = shallow(<Component1 />);
    expect(component).toMatchSnapshot();
  });

  it('should not have component2', () => {
    const component = shallow(<Component1 />);
    expect(component.find('Component2').exists()).toEqual(false);
  })

  it('should contain button getComponent2 and should have component2', ()=> {
    const component = shallow(<Component1 />);
    expect(component.instance().state.isClicked).toEqual(false);
    // eslint-disable-next-line jest/valid-expect
    expect(component.find('button').exists());
    const button = component.find('button');
    button.simulate("click");
    expect(component.instance().state.isClicked).toEqual(true);
    expect(component.find('Component2').exists()).toEqual(false);
    expect(component.find('.component-2').exists()).toEqual(false);
  });  

});